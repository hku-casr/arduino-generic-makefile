#
# Name: LIBRARY.mk
#
# Description: This header file provide library references. All available
#  libraries and their corresponding absolute path is listed except variants
#  library associated to specific Arduino board and architectural-specific
#  libraries which is to be provided in header files dedicated to respective
#  boards.
#
#  NOTE: This header file requires the "variant" variable defined in the header
#  file dedicated to a specific board. This header file is not including or
#  importing these board-specific header file but requires project Makefile to
#  quote such board-specific header file before citing this header file so the
#  "variant" variable can be inherited in this scope.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#
# Date: 27 January 2016
#

#
# The paths to the source code root directory for available architectural-
#  independent libraries
#
Ethernet  = /Users/dominichung/.arduinocdt/libraries/Ethernet
Ethernet2 = /Users/dominichung/.arduinocdt/libraries/Ethernet2
WiFi      = /Users/dominichung/.arduinocdt/libraries/WiFi/1.2.6/src
WiFi101   = /Users/dominichung/.arduinocdt/libraries/WiFi101/0.12.1/src