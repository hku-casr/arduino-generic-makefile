#
# Name: COMMON.mk
#
# Description: A header file for common toggling switches and common paths.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#
# Date: 28 January 2016
#

#
# Reference files for producing the this template
#  HDWR_FILE = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/boards.txt
#  CONF_FILE = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/platform.txt
#

#
# BASIC CONFIGURATION REGION
#
# ARDUINO_MK_BASE is the path to the root of this project of Makefile templates
# DEV_SWITCH      configures the arduino/arduino-compatible board a project is
#                  compiled for.
#
ARDUINO_MK_BASE = /Users/dominichung/Documents/Arduino/ARDMK
DEV_SWITCH      = FEATHER_M0

#
# ADVANCED CONFIGURATION REGION
#
# WARNING: The following code region is suggested not to be changed by typical
#  end-user.
#
CC_WARN_FLAGS = -w

CC_FLAGS  = -c -g -Os ${CC_WARN_FLAGS} -std=gnu11 -ffunction-sections -fdata-sections -nostdlib --param max-inline-insns-single=500 -MMD
CPP_FLAGS = -c -g -Os ${CC_WARN_FLAGS} -std=gnu++11 -ffunction-sections -fdata-sections -fno-threadsafe-statics -nostdlib --param max-inline-insns-single=500 -fno-rtti -fno-exceptions -fpermissive -MMD
AS_FLAGS  = -c -g -x assembler-with-cpp -MMD
AR_FLAGS  = rcs
ELF_FLAGS = -Os -Wl,--gc-sections -save-temps
LD_FLAGS  = -Wl,-Map,$(shell pwd)/bitstream/$(TARGET).map --specs=nano.specs --specs=nosys.specs -Wl,--cref -Wl,--check-sections -Wl,--gc-sections -Wl,--unresolved-symbols=report-all -Wl,--warn-common -Wl,--warn-section-align

BIN_FLAGS = -O binary
HEX_FLAGS = -O ihex -R .eeprom