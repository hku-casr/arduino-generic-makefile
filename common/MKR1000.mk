#
# Name: MKR1000.mk
#
# Description: A header file for inclusion to Makefiles associated for
#  compilation targeted on Arduino MKR1000.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#
# Date: 25 January 2016
#

#
# Reference files for producing the this template
#  HDWR_FILE = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/boards.txt
#  CONF_FILE = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/platform.txt
#

#
# The path to the core library and the variant sub-module that is to be added
#  into the core library archive that is central to all Arduino sketches
#
variants  = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/variants/mkr1000
cores     = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/cores/arduino $(variants)

#
# The path to libraries/binaries to the board UART interface/flashing protocol
#
CMSIS_4_PATH  = /Users/dominichung/.arduinocdt/packages/arduino/tools/CMSIS/4.5.0/CMSIS/Include
CMSIS_1_PATH  = /Users/dominichung/.arduinocdt/packages/arduino/tools/CMSIS-Atmel/1.0.0/CMSIS/Device/ATMEL

#
# The path to GNU Compiler Collection binaries serving the architecture of this
#  board
#
CXX = /Users/dominichung/.arduinocdt/packages/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1/bin/arm-none-eabi-g++
CC  = /Users/dominichung/.arduinocdt/packages/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1/bin/arm-none-eabi-gcc
AS  = /Users/dominichung/.arduinocdt/packages/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1/bin/arm-none-eabi-gcc
AR  = /Users/dominichung/.arduinocdt/packages/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1/bin/arm-none-eabi-ar
ELF = /Users/dominichung/.arduinocdt/packages/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1/bin/arm-none-eabi-gcc
OJC = /Users/dominichung/.arduinocdt/packages/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1/bin/arm-none-eabi-objcopy

#
# Board architecture-related variables
#
BOARD_MCU_ARCH = SAMD
BOARD_MCU_GEN  = cortex-m0plus
BOARD_MCU_REV  = __SAMD21G18A__
BOARD_TYPE     = SAMD_MKR1000
BOARD_F_CPU    = 48000000L

#
# Board UART interface USB identity
#
USB_VID        = 0x2341
USB_PID        = 0x804e
USB_MANUER     = "Arduino LLC"
USB_DSCPT      = "Arduino MKR1000"

#
# Board-specific flags for use in calling GNU Compiler Collection tools
#
# LNK_SCPT refers to the linker to the linker script for generating binaries
#  for this board. The referred linker script is dedicated to bootstrapping
#  an object with standard Arduino bootloader. For bootstrapping the Arduino
#  without the Arduino bootloader, replace the linker script reference below.
#
BRD_FLAGS = -mcpu=$(BOARD_MCU_GEN) -mthumb -DF_CPU=$(BOARD_F_CPU) -DARDUINO_$(BOARD_TYPE) -DARDUINO_ARCH_$(BOARD_MCU_ARCH) -D$(BOARD_MCU_REV)
USB_FLAGS = -DUSB_VID=$(USB_VID) -DUSB_PID=$(USB_PID) -DUSBCON '-DUSB_MANUFACTURER=$(USB_MANUER)' '-DUSB_PRODUCT=$(USB_DSCPT)'
LNK_SCPT  = -T$(variants)/linker_scripts/gcc/flash_with_bootloader.ld
# BRD_FLAGS = -mcpu=$(BOARD_MCU_GEN) -mthumb # NO DEFINE FLAGS FOR LINKER, BUT WILL TRY IF LINKER WILL IGNORE THE FLAGS

#
# Compiler include flags universal to all arduino sketch projects
#
MASTER_INC = -I$(CMSIS_4_PATH) -I$(CMSIS_1_PATH) $(shell if [ ! -z "$(cores)" ]; then echo $(cores) | sed -e "s/^/-I/g" | sed -e "s/ / -I/g"; fi)

#
# The path to architectural-specific libraries
#
HID       = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/libraries/HID
I2S       = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/libraries/I2S/src
SAMD_AC   = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/libraries/SAMD_AnalogCorrection/src
SPI       = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/libraries/SPI
USBHost   = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/libraries/USBHost/src
Wire      = /Users/dominichung/.arduinocdt/packages/arduino/hardware/samd/1.6.11/libraries/Wire
